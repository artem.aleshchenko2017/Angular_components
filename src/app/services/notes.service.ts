import { Injectable } from '@angular/core';
import {Note} from '../model/note';
import {merge, Observable, of, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class NotesService {

	private notes:Note[] = [];
	private active:Note|null = null;
	private notesSubject:Subject<Note[]> = new Subject<Note[]>();
	private activeNoteSubject:Subject<Note|null> = new Subject();

	public getNotes():Observable<Note[]>{
		return merge(this.notesSubject,of(this.notes));
	}

	public addNote(note:Note){
		this.notes.push(note);
		this.notesSubject.next(this.notes);
	}

	public activateNote(note: Note|null){
		this.active = note;
		this.activeNoteSubject.next(note)
	}

	public getActiveNote():Observable<Note|null>{
		return this.activeNoteSubject;
	}

	deleteNote(note:Note|null){
		if (this.active==note) this.activateNote(null);
		
		this.notes = this.notes.filter(n=>n!==note);
		this.notesSubject.next(this.notes);
		
	}
}
