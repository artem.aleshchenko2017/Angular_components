import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './components/app.component';
// ../components/app.component
import { FormsModule } from '@angular/forms';
import { ActiveNoteComponent } from './components/active-note/active-note.component';
import { AddNoteFormComponent } from './components/add-note-form/add-note-form.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    ActiveNoteComponent,
    AddNoteFormComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
