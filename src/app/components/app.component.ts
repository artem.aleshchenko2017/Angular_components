import { Component } from '@angular/core';
import {Note} from "../model/note";
import {NotesService} from '../services/notes.service';
import { Inject } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {


	constructor(@Inject(NotesService) private noteService:NotesService){
		
	}


}
