import { Component, Inject, OnInit } from '@angular/core';
import { Note } from '../../model/note';
import { NotesService } from '../../services/notes.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
	activeNote: any;
	notes:Note[] = [];

  constructor(@Inject(NotesService) private noteService:NotesService) { 
  	noteService.getNotes().subscribe(n=>this.notes=n);
    noteService.getActiveNote().subscribe(n=>this.activeNote=n);
  }

  changeNote(note : any){
    this.noteService.activateNote(note);
  }

  deleteNote(note : any){
    this.noteService.deleteNote(note);
  }
}
